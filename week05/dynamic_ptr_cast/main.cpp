#include <iostream>
#include <vector>
#include <memory>

struct B { virtual ~B() = default; };
struct D1 : B { };
struct D2 : B { };
struct D3 : B { };

int main()
{
   // Store derived objects as base class pointers
   std::vector<std::shared_ptr<B>> vec  = { std::make_shared<D1>()
                                          , std::make_shared<D2>()
                                          , std::make_shared<D3>()
                                          };


   // Use dynamic ptr cast to determine which derived class it is
   for(auto const &elem : vec)
   {
      std::cout << elem;
      if(std::dynamic_pointer_cast<D1>(elem))
         std::cout << " is a D1 object" << std::endl;
      else if(std::dynamic_pointer_cast<D2>(elem))
         std::cout << " is a D2 object" << std::endl;
      else if(std::dynamic_pointer_cast<D3>(elem))
         std::cout << " is a D3 object" << std::endl;
      else
         std::cout << " is an unknown object" << std::endl;
   }


}
