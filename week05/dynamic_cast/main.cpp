#include <iostream>

struct B { virtual ~B() = default; };
struct D1 : B { };
struct D2 : B { };
struct D3 : B { };

int main()
{
   // Store derived objects as base class pointers
   B* parr[]  =  { new D1
                 , new D2
                 , new D3
                 };


   // Use dynamic cast to determine which derived class it is
   for(auto elem : parr)
   {
      std::cout << elem;
      if(dynamic_cast<D1*>(elem))
         std::cout << " is a D1 object" << std::endl;
      else if(dynamic_cast<D2*>(elem))
         std::cout << " is a D2 object" << std::endl;
      else if(dynamic_cast<D3*>(elem))
         std::cout << " is a D3 object" << std::endl;
      else
         std::cout << " is an unknown object" << std::endl;
   }


   // Free memory
   for(auto &elem : parr)
   {
      delete elem;
      elem = nullptr;
   }
}
