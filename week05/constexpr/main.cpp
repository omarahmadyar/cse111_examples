#include <iostream>
#include <chrono>

constexpr uint64_t cfib(uint64_t x)
{
   if(x < 1) return 0;
   else if(x == 1) return 1;
   return cfib(x-1) + cfib(x-2);
}

uint64_t fib(uint64_t x)
{
   if(x < 1) return 0;
   else if(x == 1) return 1;
   return cfib(x-1) + cfib(x-2);
}

int main()
{
   using namespace std::chrono;
   using clock = steady_clock;
   constexpr auto x = 34;


   std::cout << "fib call ------------------------------\n";
   auto start = clock::now();
   auto r = fib(x);
   auto fib_time = clock::now() - start;
   std::cout << "Result = " << r << '\n';
   std::cout << "Runtime = " << duration_cast<nanoseconds>(fib_time).count() << "ns\n\n";

   std::cout << "cfib call -----------------------------\n";
   start = clock::now();
   constexpr auto c = cfib(x);
   fib_time = clock::now() - start;
   std::cout << "Result = " << c << '\n';
   std::cout << "Runtime = " << duration_cast<nanoseconds>(fib_time).count() << "ns\n";

   return 0;
}
