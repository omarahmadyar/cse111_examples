#include <iostream>

// You can define your own objects to throw as an exception
struct MyExcept {
   virtual const char* what() const noexcept { return "MyExcept"; }
};

// We can derive more structs that can be caught by catching the base struct
struct SomeError : MyExcept {
   const char* what() const noexcept override { return "Some error occurred"; }
};
struct Scream : MyExcept {
   const char* what() const noexcept override { return "AHHHHHHHHHH"; }
};

// Or you can inherit from an already existing exception object
struct BetterExcept : std::runtime_error {
   explicit BetterExcept(std::string const & str) : std::runtime_error(str) {}
};

// FOO
void foo() {
   throw MyExcept();
   throw SomeError();
   throw Scream();
   throw std::runtime_error("Some runtime error");
   throw BetterExcept("lmao");
}

// Main ------------------------------------------------------------------------
int main()
{
   try
   {
      foo();
   }
   catch (MyExcept const & e) {
      std::cerr << e.what() << std::endl;
   }
//   catch (std::runtime_error const & e) {
//      std::cerr << e.what() << std::endl;
//   }

   return 0;
}
