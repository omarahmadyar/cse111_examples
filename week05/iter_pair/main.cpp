#include <iostream>

template<typename Iter>
std::ostream& operator<<(std::ostream& out, std::pair<Iter,Iter> const &p)
{
   out << "My custom operator<<\n";
   for(auto it = p.first; it != p.second; ++it)
      out << *it << '\n';
   return out;
}

int main()
{
   int arr[] = {0,1,2,3,4,5,6,7,8,9};
   std::cout << std::make_pair(std::begin(arr), std::end(arr));
}
