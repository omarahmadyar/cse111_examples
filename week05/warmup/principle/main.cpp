#include <iostream>

struct PrintHello {
   void operator()(std::string name = "fren") const {
      std::cout << "Hello, " << name << "!\n";
   }
};

int main()
{
   std::string myname = "Omar";

   // Default Construct the PrintHello struct
   PrintHello structhello;

   // Lambda that returns void and requires a parameter
   auto printhello = [](std::string name){ std::cout<<"Hello, "<<name<<"!\n"; };

   // Lambda that returns void, takes no parameter, but passes a reference
   auto otherhello = [myname](){ std::cout<<"Hello, "<<myname<<"!\n"; };


   // Call functions/lambdas
   std::cout << "printhello lambda (w/ param) call -------------------------\n";
   printhello(myname);

   std::cout << "\notherhello lambda (w/o param) call ------------------------\n";
   otherhello();

   std::cout << "\nPrintHello class operator() call (w/ param) call ----------\n";
   structhello(myname);

   std::cout << "\nconstruct temp PrintHello and call operator() -------------\n";
   PrintHello{}(myname);


   std::cout << '\n';
   return 0;
}
