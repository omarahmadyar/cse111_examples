#include <iostream>
#include <vector>

// Unary         class that has a unary operator()
// iIter         iterators from input range
// oIter         iterator from output range

// beg/end       range from input
// obeg          starting point from output iterator
// op            a Unary object that can be used like a function
template<typename Unary, typename iIter, typename oIter>
oIter map(iIter beg, iIter end, oIter obeg, Unary op = Unary())
{
   //TODO: This is your warmup activity
}





struct MyOP {
   float operator() (int x) const noexcept { return 1.5f * x; }
};


int main()
{
   /* We want to map a pair of ints into a pair of floats */
   std::vector<int> vec1 = {0,1,2,3,4,5,6,7,8,9};
   std::vector<float> vec2 (vec1.size());


   //fun|template| param1 | param2     |    param3
   map<MyOP>(vec1.cbegin(), vec1.cend(), vec2.begin());

   for(decltype(vec1.size()) i{}; i < vec1.size(); ++i)
      std::cout << vec1.at(i) << " -> " << vec2.at(i) << '\n';
   std::cout << "\n\n";


   //fun| param1      | param2     | param3      |    param4
   map(vec1.cbegin(), vec1.cend(), vec2.begin(), [](int x){return 1.5f*x;});

   for(decltype(vec1.size()) i{}; i < vec1.size(); ++i)
      std::cout << vec1.at(i) << " -> " << vec2.at(i) << '\n';
   std::cout << "\n\n";
}
