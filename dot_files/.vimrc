:set tabstop=3
:set shiftwidth=3
:set softtabstop=0
:set expandtab
:set smarttab
:set list
:set listchars=tab:\\·,trail:·

:set number

:set autoindent
:set cindent
:set ff=unix

:syntax on
