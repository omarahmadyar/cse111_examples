#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
PS1='[\u@\h \W]\$ '

alias ls='ls --color=auto'
alias la='ls -lA --color=auto'
alias ll='ls -l --color=auto'
alias sshunix='ssh oahmadya@unix.ucsc.edu'
alias pacman='pacman --color=auto'
alias please='sudo $(history -p \!-1)'
alias ayy='echo lmao'
alias k='cd && clear && screenfetch"'
alias d='cd ~/Downloads/'
alias t='cd ~/temp'
alias s='cd ~/MSI/week02'
alias mansplain='man'
alias update="sudo ls >/dev/null; time sudo pacman -Syu"
alias shutdown='shutdown --no-wall'
alias bashrc='vim ~/.bashrc && source ~/.bashrc'
alias weather='curl "wttr.in/Santa Cruz,CA"'

ccr() {
   clear && g++ *.cpp -std=c++20 $@ && ./a.out
}

stfu() {
   exec "$@" 1>/dev/null 2>/dev/null &
}

# Customize Pager
default=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
purple=$(tput setaf 5)
orange=$(tput setaf 9)

# Less colors for man pages
export PAGER=less
# Begin blinking
export LESS_TERMCAP_mb=$red
# Begin bold
export LESS_TERMCAP_md=$orange
# End mode
export LESS_TERMCAP_me=$default
# End standout-mode
export LESS_TERMCAP_se=$default
# Begin standout-mode - info box
#export LESS_TERMCAP_so=$purple
# End underline
export LESS_TERMCAP_ue=$default
# Begin underline
export LESS_TERMCAP_us=$green
# End Customize Pager

PATH="$PATH:."; export PATH;
EDITOR="vim"; export EDITOR;

