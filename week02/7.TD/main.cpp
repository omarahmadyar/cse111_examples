#include <iostream>

template <typename T>
class TD;

using namespace std::literals;
int main()
{
   // Try making x a bunch of different values
   auto x = 1ull;
   TD<decltype(x)> test;
   return 0;
}

/*
   How you can use a declaration of a class
   with no definition to determine the type of variables.
   If you're unsure of what auto deduces to, you can use
   this trick to get the type of the variable printed to you
   in an compilation error.

   btw: decltype deduces to the exact type of the variable
   it was given
*/
