#include <iostream>

template<typename T>
class Wrapper
{
private:
   T data_{};

public:
   Wrapper(T val) : data_(val) { }

   template<typename G>
   friend std::ostream& operator<<(std::ostream&, Wrapper<G> const &);
};

template<typename T>
std::ostream& operator<<(std::ostream& out, Wrapper<T> const & wrap)
{
   return out << wrap.data_;
}

int main()
{
   Wrapper<int> one(1);
   Wrapper<int> two(2);
   Wrapper<int> three(3);
   Wrapper<int> four(4);
   Wrapper<int> five(5);

   /* this works because every call to operator<< returns a reference
      to std::cout so it all chains together
   */
   std::cout << one << two << three << four << five << std::endl;


   return 0;
}
/*
   Here's what happens after every call to operator<<
   std::cout << one << two << three << four << five << std::endl;
   std::cout << two << three << four << five << std::endl;
   std::cout << three << four << five << std::endl;
   std::cout << four << five << std::endl;
   std::cout << five << std::endl;
   std::cout << std::endl;
   std::cout;
*/
