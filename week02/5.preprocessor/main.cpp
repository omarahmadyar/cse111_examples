#include <iostream> //this line is replaced with a copy/paste of iostream


int main()
{

   std::cout << "Hello, ";
   #ifndef SHH //Start preprocessor if not statement
   std::cout << "\nAHHHHHHHHHHHHHHHH\n";
   #endif // End if not statement
   std::cout << "World!\n";

   return 0;
}

/*
   Compile this into Assembly.
   If SHH is defined, it's not only that
   AHHHH doesn't get printed, AHHHH
   doesn't even exist.  That block is
   removed from the code before it reaches
   the compiler
*/

