#include <iostream>

// How to create your own namespace
namespace omar
{
   std::string name()
   {
      return "Omar Ahmadyar";
   }
}

int main()
{
   std::cout << omar::name() << std::endl;

   using namespace omar;  //search the omar namespace as well
   std::cout << name() << std::endl;

   using namespace std;  //search the std namespace as well
   cout << name() << endl;
}
