#include <iostream>

template<typename T>
class TD;

int main()
{
   auto a = 5;                   //int = literal
   auto b = a;                   //int = int
   auto &c = b;                  //int& = int
   auto d  = c;                  //int = int&
   decltype(auto) e = c;         //int& = int&
   auto &f = e;                  //int& = int&

   auto const x = 5;             //int = literal
   auto y = x;                   //int = int const
   auto& z = x;                  //int const & = int const


   return 0;
}

/*
   Auto isn't magic.  It deduces to the base/decayed type
   of whatever is on the right side. (eg: no ref, const, volatile)

   If you want it to deduce to the exact type of the right side,
   use decltype(auto)
*/


