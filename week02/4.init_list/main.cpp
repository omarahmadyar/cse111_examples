#include <iostream>

// All Special Member functions defined to print statements
struct Type
{
   Type() { std::cout << "     > Default Constructor\n"; }
   Type(Type const &) { std::cout << "     > Copy Constructor\n"; }
   Type(Type const &&) { std::cout << "     > Move Constructor\n"; }
   Type& operator=(Type const &) { std::cout << "     > Copy Assignment\n"; return *this; }
   Type& operator=(Type &&) { std::cout << "     > Move Assignment\n"; return *this; }
   ~Type() { std::cout << "     > Destructor\n"; }
};

class Obj
{
   Type data_;

public:
   Obj(Type const & t)// : data_(t)
   {                                  // What happens if you construct data_
      data_ = t;                      // in an init list vs in the body?
   }                                  // Consult the stdout

   Obj(Type &&t) : data_(std::move(t))
   {
   }

};


int main()
{
   std::cout << "Create Type instance\n";
   Type x;
   std::cout << "Create Obj instance -----------------------------------\n";
   Obj oj(x);
   std::cout << "Done Creating Obj instance ----------------------------\n";

   return 0;
}

