#include <iostream>

void myprint(std::string const & str) { std::cout << str << std::endl; }


template <typename T>
class TD;

int main()
{
   auto cstr = "Hello everyone";

   //TD<decltype(cstr)> test;

   /* myprint requires an std::string but cstr is a char[]
      An std::string is implicitly constructed from cstr
   */
   myprint(cstr);
   return 0;
}
