#include <iostream>

class bigint
{
   int x_;

public:
   bigint() = default;
   bigint(int x) : x_(x) { }


   bigint operator+(bigint const & rhs) const { return this->x_ + rhs.x_; }
   bigint operator-(bigint const & rhs) const { return this->x_ - rhs.x_; }
   bigint operator*(bigint const & rhs) const { return this->x_ * rhs.x_; }
   bigint operator/(bigint const & rhs) const { return this->x_ / rhs.x_; }
   bigint operator%(bigint const & rhs) const { return this->x_ % rhs.x_; }
   bigint& operator++() { ++x_; return *this; }
   bigint operator++(int) { auto temp = x_; ++x_; return temp; }
   bigint& operator--() { --x_; return *this; }
   bigint operator--(int) { auto temp = x_; --x_; return temp; }
   bigint& operator+() { return *this; }
   bigint operator-() { auto temp = x_ * -1; return temp; };

   operator int() { return x_; }  //try making this explicit
   explicit operator bool() { return x_ != 0; } //and this implicit

};




int main()
{
   bigint x = 5;
   bigint y = 3;
   auto z = y + x;
   /*
      there is no operator<<(ostream&, bigint const&) defined so z must be
      implicitly converted to something else for it to print
   */
   std::cout << z << std::endl;
   return 0;
}
