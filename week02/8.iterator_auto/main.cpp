#include <iostream>
#include <vector>
#include <map>

int main()
{
   // Vector iterator auto vs manual
   std::vector<int> vec;
   std::vector<int>::const_iterator i_ = vec.cbegin();
   auto i = vec.cbegin();


   // For each loop over map
   std::map<std::string, int> map = {  {"one"  , 1}
                                    ,  {"two"  , 2}
                                    ,  {"three", 3}
                                    };

   // standard pair
   std::cout << "First\n";
   for(std::pair<const std::string, int> const & it : map)
      std::cout << it.first << " = " << it.second << '\n';

   // map value_type
   std::cout << "\nSecond\n";
   for(std::map<std::string, int>::value_type const & it : map)
      std::cout << it.first << " = " << it.second << '\n';

   // decltype value_type
   std::cout << "\nThird\n";
   for(decltype(map)::value_type const &it : map)
      std::cout << it.first << " = " << it.second << '\n';

   // auto
   std::cout << "\nFourth\n";
   for(auto const & it : map)
      std::cout << it.first << " = " << it.second << '\n';

   return 0;
}

/*
   For loops over the same map.  The last method is the best, not
   only because it's less verbose, but because it will still work
   even if you change the type of the map or perhaps you have a typo
*/
