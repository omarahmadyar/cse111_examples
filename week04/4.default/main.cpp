#include <iostream>
#include <memory>


template<typename T>
struct Wrapper {
   std::unique_ptr<T> ptr = std::make_unique<T>();

   Wrapper() = default;

   template<typename G>
   Wrapper(G &&val) {
      *ptr = std::forward<G>(val);
   }
};


int main()
{
   Wrapper<int> x(5);
   std::cout << *x.ptr << std::endl;
   return 0;
}

/*
   You can default initalize class members in the declaration.
   This is useful so you don't have to specify a variable
   in your inializer lists for every single constructor.

   However, initializer lists take priority, and the default
   initialization will not take place if that parameter is constructed
   in an initializer list.  If the constructor you call does not specify
   the member in an initializer list, then it will fall back to the
   default initialization you specified.
*/
