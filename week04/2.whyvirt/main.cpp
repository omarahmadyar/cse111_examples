#include <iostream>
#include <vector>

/*
   Why do we use virtual?
   And why do we use override?

   Also this example is using raw pointers instead
   of smart pointers so you can see the old way of doing this
*/

struct Human {
   void speak() const {
      std::cout << "I am totally Human -- beep boop --\n";
   }
};

struct Parent : Human {
   void speak() const {
      std::cout << "Go to bed\n";
   }
};

struct Child : Human {
   void speak() const {
      std::cout << "I wanna play\n";
   }
};

int main()
{
   std::vector<Human*> vec;
   vec.push_back(new Human);
   vec.push_back(new Parent);
   vec.push_back(new Child);

   for(auto &x : vec)
      x->speak();

   for(auto ptr : vec)
      delete ptr;
   return 0;
}

/*
   1. The Parent and Child objects and stored as Human pointers.
      Because of this calls to speak() route to the Parent speak().
      To make it look up the type dynamically and route to the proper
      speak(), make the speak() in the base class virtual.  That way
      whenever you call speak() on a parent pointer, it will double
      check the type at runtime.

   2. Try changing the signature of speak() in any of the derived
      classes.  Maybe by removing the const.  Run the program and
      what happens?  Functions won't override if they don't match
      exactly.  Specify the override keyword to cause a compilation
      error if you accidentally mess up the function signature.

   3. I used raw pointers so I have to manually deallocate at the end.
*/
