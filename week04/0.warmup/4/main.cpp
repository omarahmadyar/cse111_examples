#include <iostream>
#include <memory>

void baz(int *);

// Does foo leak memory?
void foo()
{
   auto obj = std::make_unique<int>(2345); //type obj is std::unique_ptr<int>
   baz(obj.get()); //get() accesses the pointer unique_ptr is storing
}

















































int main() {
   try {
      foo();
   } catch(...) {
   }
   return 0;
}

void baz(int *o) {
   throw *o;
}
