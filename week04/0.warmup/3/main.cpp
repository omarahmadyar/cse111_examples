#include <iostream>

void baz(int *);

// Does foo() leak memory
void foo()
{
   int *obj = new int(2345);
   baz(obj); //doing work
   delete obj;
}

















































int main() {
   try {
      foo();
   } catch(...) {
   }
   return 0;
}

void baz(int *o) {
   throw *o;
}
