#include <memory>


// Does foo() leak memory?
void foo()
{
   auto obj = std::make_unique<int>();
   // do some work in foo
}


















































int main()
{
   foo();
}
