#include <memory>

struct Node {
   std::shared_ptr<Node> ptr;
   std::weak_ptr<Node> wptr;
   Node(Node *x = nullptr) : ptr(x) { }
};

int main()
{
   auto x = std::make_shared<Node>();
   auto y = std::make_shared<Node>();
   auto z = std::make_shared<Node>();
   x->ptr = y;
   y->ptr = z;
   z->ptr = x;


   return 0;
}

/*
   Draw out this example yourself line by line.
   Keep track of changes to strong and weak count along with calls to new and delete
   with each line.

   Shared_ptr maintains a strong and weak count.
   - Strong count is how many shared_ptr's are pointing at a pointer.
   - Weak count is how many weak_ptr's are pointing to a pointer.

   Shared_ptr must allocate its own heap memory to maintain a strong and weak count.
   - The memory shared_ptr is managing is deleted when the strong count hits 0
   - The internal memory shared_ptr needs is deleted when both strong and weak count hit 0

   Whenever a shared_ptr is constructed strong_count goes up by 1
   Whenever a shared_ptr is destructed/reset/changed strong_count goes down by 1
   The above 2 lines are true for weak_ptr but for weak_count instead of strong_count

   weak_ptr's can be used to break cycles since they do not modify the strong count.
   weak_ptr's can be promoted to shared_ptr's for temporary use using the lock() member.

   Try to break the cycle in this example using a weak_ptr instead of a shared_ptr.
*/
