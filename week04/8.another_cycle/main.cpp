#include <memory>

struct Node {
   std::shared_ptr<Node> ptr;
};

int main()
{
   auto x = std::make_shared<Node>();
   x->ptr = x;
}

/*
   This is just another example of a cycle
*/
