#include <iostream>
#include <memory>

int main()
{
   std::unique_ptr<int> x{new int(11)};
   std::unique_ptr<int> y{x.get()};
}

/*
   Here are two unique_ptr's referring to the same pointer.
   Not very unique huh.  Let's see what happens when they both
   destruct.
*/
