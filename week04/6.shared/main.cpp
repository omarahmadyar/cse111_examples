#include <iostream>
#include <memory>

int main()
{
   auto x = std::make_shared<int>(20);
   auto y = x;
   //std::shared_ptr<int> z{x.get()};  //very bad
   return 0;
}


/*
   unique_ptr's are more simple and efficient than
   shared_ptrs, but if you want multiple smart pointers
   pointing to one raw pointer, then you're gonna want
   shared_ptr's.

   Clone shared_ptr's by copy assigning or constructing
   them from each other.  Never attempt to construct
   a smart pointer from a raw pointer that is already
   being managed.

   If you uncomment the very bad line, z will construct a
   shared pointer from the same raw pointer as x and y, but
   x and y don't know about z, and z doesn't know about x and y.
   If they can't coordinate with each other then we'll end
   up with multiple calls to delete.
*/
