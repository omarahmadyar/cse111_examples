#include <iostream>
#include <vector>
#include <memory>


struct Animal {
   virtual void speak() const = 0;
   virtual bool isDog() const { return false; }
   virtual bool isCat() const { return false; }
};

struct Dog : Animal {
   void speak() const override {
      std::cout << "Bark" << std::endl;
   }
   bool isDog() const override { return true; }
};

struct Cat : Animal {
   void speak() const override {
      std::cout << "Meow" << std::endl;
   }
   bool isCat() const override { return true; }
};

int main()
{
   std::vector<std::unique_ptr<Animal>> vec;
   //vec.emplace_back(std::make_unique<Animal>()); //can't instantiate an abstract class
   vec.emplace_back(std::make_unique<Dog>());
   vec.emplace_back(std::make_unique<Cat>());

   for(auto &animal : vec) //what happens if you remove this reference?
   {
      animal->speak();

      std::cout << "Animal is ";
      if(animal->isDog()) std::cout << "Dog";
      else if(animal->isCat()) std::cout << "Cat";
      else std::cout << "what the hell!?";
      std::cout << "\n\n";
   }

}

/*
   If you remove the reference in the above loop, what will happen is it will attempt
   to copy construct the std::unique_ptr<Animal> object known as animal instead of
   just taking a reference to the original.  This causes a compilation error because
   the copiers for unique_ptr's are deleted because unique_ptr's must be unique.
*/
