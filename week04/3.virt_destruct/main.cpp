#include <memory>

class Parent {
   std::unique_ptr<int> x_ = std::make_unique<int>();

// All 6 Special Member Functions
public:
   // Parent()                          = default;
   // Parent(Parent const &)            = default;
   // Parent(Parent &&)                 = default;
   // Parent& operator=(Parent const &) = default;
   // Parent& operator=(Parent &&)      = default;
   // ~Parent()                         = default;
};

class Child : public Parent {
   std::unique_ptr<int> y_ = std::make_unique<int>();
};


int main()
{
   std::unique_ptr<Parent> test  =  std::make_unique<Child>();

   return 0;
}


/*
   Parent has member x_
   Child has members y_ and x_ (inherited).
   If you create a child object and store it as a Parent pointer,
   then whenever delete gets called on it, it will call the Parent
   destructor instead of the Child one unless the destructor is virtual
   in which case it will check the actual type during run time.


   Specifying any of the special member functions tends to result in
   others being suppressed, so if you specify one (even as default),
   then you should specify them all.
*/
