#include<inttypes.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<arpa/inet.h>

int main()
{
   uint32_t x = 0x12345678;
   int fd = open("out.bin", O_TRUNC | O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR);
   write(fd, &x, sizeof x);
   close(fd);
   return 0;
}
