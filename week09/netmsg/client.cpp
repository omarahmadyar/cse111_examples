#include <iostream>
#include <cstring>
#include <array>

#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <netinet/in.h>
#include<arpa/inet.h>
#include <unistd.h>


int main()
{
   using std::runtime_error;
   using namespace std::literals;
   constexpr uint16_t PORT = 1234;

   // Get User Input
   std::cout << "Write a message:" << std::endl;
   std::array<char, 0x1000> buffer;
   uint32_t num_bytes = 0;
   {
      int x{};
      while((x = read(0, buffer.data() + num_bytes, 0x1000 - num_bytes)) > 0)
         num_bytes += x;
      buffer[num_bytes - 1] = 0;
   }

   // Connect to Server
   int server = socket(AF_INET, SOCK_STREAM, 0);
   if(server < 0) throw runtime_error("Couldn't open socket: "s + strerror(errno));

   struct sockaddr_in addr{};
   addr.sin_family = AF_INET;
   addr.sin_port = htons(PORT);
   if(inet_aton("127.0.0.1", &addr.sin_addr) != 1)
      throw runtime_error("Couldn't create sockaddr");

   if(connect(server, reinterpret_cast<sockaddr*>(&addr), sizeof addr) != 0)
      throw runtime_error("Couldn't connect to server: "s + strerror(errno));

   // Tell Server how many bytes you're sending
   uint32_t net_bytes = htonl(num_bytes);
   if(send(server, &net_bytes, sizeof net_bytes, 0) != 4)
      throw runtime_error("Couldn't send num bytes");

   // Send Message
   {
      std::size_t sent = 0;
      while(sent < num_bytes)
      {
         ssize_t x{};
         if(x = send(server, buffer.data() + sent, num_bytes - sent, 0); x <= 0)
            throw runtime_error("Couldn't send right amount of bytes: "s+ strerror(errno));
         sent += x;
      }
   }


   close(server);
}
