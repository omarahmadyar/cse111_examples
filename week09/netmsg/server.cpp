#include <iostream>
#include <exception>
#include <memory>
#include <cstring>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>


int main()
{
   using std::runtime_error;
   using namespace std::literals;
   constexpr uint16_t PORT = 1234;

   // Open Socket
   int server = socket(AF_INET, SOCK_STREAM, 0);
   if(server < 0) throw runtime_error("Couldn't open socket: "s + strerror(errno));

   // Setup Address for Server
   struct sockaddr_in addr{};
   addr.sin_family = AF_INET;
   addr.sin_port = htons(PORT);
   if(inet_aton("0.0.0.0", &addr.sin_addr) != 1)
      throw runtime_error("Couldn't setup sockaddr");

   // Bind Socket to Address
   if(bind(server, reinterpret_cast<sockaddr*>(&addr), sizeof addr) != 0)
      throw runtime_error("Couldn't bind socket to address: "s + strerror(errno));

   // Listen on Socket
   if(listen(server, SOMAXCONN) != 0)
      throw runtime_error("Couldn't listen on socket: "s+strerror(errno));

   // Accept Client Connection
   int client = accept(server, nullptr, nullptr);
   if(client < 0)
      throw runtime_error("Failed to accept connection: "s+strerror(errno));


   // Read 4 bytes and interpret as content-length
   uint32_t num_bytes;
   if(4 != recv(client, &num_bytes, sizeof num_bytes, 0))
      throw runtime_error("Could not recv num_bytes: "s + strerror(errno));
   num_bytes = ntohl(num_bytes);

   // Receive rest of message
   auto buffer = std::make_unique<uint8_t[]>(num_bytes + 1);
   {
      std::size_t received = 0;
      while(received != num_bytes)
      {
         auto rec = recv(client, buffer.get() + received, num_bytes - received, 0);
         if(rec <= 0)
            throw runtime_error("Recv'd wrong number of bytes or Error in recv");
         received += rec;
      }
   }

   // Close Client Connection and Stop Server
   close(client);
   close(server);

   // Null Terminate and treat as C-String
   buffer[num_bytes] = 0;

   // Print Message to Terminal
   std::cout << buffer.get() << std::endl;

}


