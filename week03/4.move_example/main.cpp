#include <iostream>


struct Data {
  // Members/Data
  int *x{};

  // Default Constructor
  Data(int i = 0) : x(new int(i)) { }

  // Copy operator=
  Data& operator=(Data const &rhs) {
    std::cout << "copy op\n";

    if(x == nullptr) x = new int;
    *x = *rhs.x;

    return *this;
  }

  // Move operator=
  Data& operator=(Data &&rhs) {
    std::cout << "move op\n";

    auto temp = x;
    x = rhs.x;
    rhs.x = nullptr;
    delete temp;

    return *this;
  }

  // Destructor
  ~Data() { delete x; }
};

// Print overload
std::ostream& operator<<(std::ostream& out, Data const &data) {
   out << static_cast<void*>(data.x) << " = ";
   if(data.x == nullptr) out << "nullptr";
   else out << *data.x;
   return out;
}


int main()
{
   std::cout << "Construct 2 data objects----------------\n";
   Data a(5);
   Data b;
   std::cout << a << '\n' << b << "\n\n";

   std::cout << "Copy a into b --------------------------\n";
   b = a;
   std::cout << a << '\n' << b << "\n\n";

   std::cout << "construct new Data then move it to a ---\n";
   a = Data(19);
   std::cout << a << '\n' << b << "\n\n";

   std::cout << "a is a lval, but cast it to an rval to move\n";
   b = std::move(a);
   std::cout << a << '\n' << b << "\n\n";
}

/*
   Moving is useful because you can steal the data of an object
   instead of copying it.  You would want to steal or move the data
   if the object is about to go out of scope, or maybe you just don't
   need it anymore.

   Here's a basic class that has move and copy operations defined.  Take
   a look at how it works, and what the stdout looks like.  Note the pointer
   stealing.


   Here's an example from assignment 1 where you would want to move
   ubigint operator+ (ubigint const &) const {
      std::vector<uint8_t> result;
      // Fill result with the answer ...


      // You have to return a ubigint, not a vector, and there
      // is no ubigint(std::vector) constructor, so...

      ubigint retVal;
      retVal.uvalue = std::move(result); // cast to rval, so vector does a
      return retVal; //return ubigint    // O(1) move instead of a O(n) copy
   }
*/
