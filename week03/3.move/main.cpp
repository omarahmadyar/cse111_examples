#include <iostream>


// This function takes a string lval reference
void foo(std::string const &) {
  std::cout << "foo lval ref" << std::endl;
}

// This function takes a string rval reference
void foo(std::string &&) {
  std::cout << "foo rval ref" << std::endl;
}


using namespace std::literals;
int main()
{
  std::string x = "lmao";
  foo(x);
  foo(   std::move(x)   );
  foo(std::string("lmao"));
  foo("lmao"s);
  return 0;
}

/*
   If an object has a variable name you can refer to it by, then
   it is an lvalue.  If the object has no name, then it is an rvalue.

   "lmao"s   -- this is a string rvalue
   auto x = "lmao"s   -- x is an lvalue since it's a variable name
   std::string("lmao")  -- no variable name so it's an rvalue

   You want to be able to differentiate between rvalues and lvalues
   because lval ref's should be kept const so you don't modify the var
   uknown to the client, but with rval references you are free to
   modify the object because you assume it's going to go out of scope
   anyways so you might as well modify it if it makes your life easier

   You can static_cast lvals to rvals by using std::move.  A basic
   move implementation looks like this

   template<typename T>
   decltype(auto) move(T&& val)
   {
      using retType = std::remove_reference_t<T>;
      return static_cast<retType&&>(val);
   }
*/

