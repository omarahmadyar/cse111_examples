#include <iostream>

// String lval
void bar(std::string const &) {
  std::cout << "bar lval" << std::endl;
}

// String rval
void bar(std::string &&) {
  std::cout << "bar rval" << std::endl;
}


// Templatized Function That takes a Universal/Forwarding Reference
template<typename T>
void foo(T&& x)
{
  std::cout << "foo" << std::endl;
  bar(std::forward<T>(x));
}


int main()
{
  using namespace std::literals;
  auto x = "hello"s; //construct std::string using literal string operator

  foo(x); //lval call

  std::cout << '\n';
  foo("hello"s);  //rval

  std::cout << '\n';
  foo(std::move(x)); // x casted to rval, never use x again after a move op

  return 0;
}


/*
   foo can take a rvalue or an lvalue reference, however, since now the
   object is no longer a nameless variable (it can be referred to by the
   parameter name x), it is perceived as an lvalue reference.  To fix this,
   we must use std::forward which will cast it back into an lvalu/rvalue
   depending on what it was.

   bar is overloaded, one takes an lval, the other takes an rval
   If you give foo an rval, it goes to the bar rval.
   If you give foo an lval, it goes to the bar lval.

   However if you take away the forward in foo, since x is now named,
   it will always go to the lval
*/
