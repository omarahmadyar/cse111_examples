inline int plusone(int x)
{
  return x + 1;
}


int main()
{
  volatile auto x = 5;
  x = plusone(x);
  return 0;
}

/*
   Try taking away/removing the inline and compiling to assembly
   Compiler will still probably inline it so specify -O0, or -O3
   depending on whether you want it inlined or not.
   Read through the assembly.  Something non-inlined should look
   like the below.

   Note: compile to assembly by specifying the -S flag
*/


//       mov %EBP-4 %EAX
//       mov %EAX %EBP-4
//       call plusone
//       
//       
//       
//       
//       plusone:
//       push %EBP
//       mov %EBP %ESP
//       sub %ESP 4
//       
//       mov %EAX %EBP-4
//       add %EAX 1
//       
//       move %ESP %EBP
//       pop %EBP
//       ret
